"use strict";

/**
 * Handlebars hello world example
 */

const express = require('express');
const path = require('path');
const handlebars = require('express-handlebars');

const app = express();

// configure handlebars as template engine
app.engine('handlebars', handlebars({
    helpers: {
        capitalize: (s) => s.split(' ').map((e) => 
                        e.charAt(0).toUpperCase() + e.slice(1).toLowerCase())
                        .join(' '),
        userAge: (birthyear) => (new Date()).getFullYear() - birthyear, 
        equals: (a, b) => a == b
    }
}));
app.set('view engine', 'handlebars');
app.set('views', path.resolve(__dirname, 'views'));

// static route
app.use('/static', express.static(path.join(__dirname, 'static')));

app.get('/userprofile/:id', (req, res) => {
    let profiles = {
        "1": {
            id: 1,
            name: "joao silva",
            birthyear: 1992, 
            career: 'professional surfing',
            bio: '<b>Joao Silva</b> is one the top surfers in Brazil.'
        }, 
        "2": {
            id: 2,
            name: "maria santos",
            birthyear: 1987, 
            career: 'customer relationship management',
            bio: '<em>Maria Santos</em> is a workaholic who loves cats.'
        }
    };

    if (profiles[req.params.id] == undefined)
        res.render('error', {
            type: 'unknown_user',
            params : {
                id: req.params.id
            }
        });
    else
        res.render('profile', profiles[req.params.id]);
});


app.listen(3000, () => {
    console.log('Server listening at port 3000');
});