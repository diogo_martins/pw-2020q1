"use strict";

/**
 * Handlebars hello world example
 */

const express = require('express');
const path = require('path');
const handlebars = require('express-handlebars');

const app = express();

// configure handlebars as template engine
app.engine('handlebars', handlebars({defaultLayout: 'master'}));
app.set('view engine', 'handlebars');
app.set('views', path.resolve(__dirname, 'views'));

// route rendered with a handlebars template
app.get('/hello/:fname/:lname', (req, res) => {
    res.render('index', {
        fname: req.params.fname, 
        lname: req.params.lname
    });
});

app.get('/hello2/:fname/:lname', (req, res) => {
    res.render('index', {
        layout: 'alternate',
        fname: req.params.fname,
        lname: req.params.lname
    });
});


app.listen(3000, () => {
    console.log('Server listening at port 3000');
});