"use strict";

const express = require('express');
const path = require('path');
const app = express();

// set ejs as template engine
app.set('view engine', 'ejs');
// set the default view lookup folder
app.set('views', path.resolve(__dirname, 'views'));

// static route
app.use('/static', express.static(path.join(__dirname, 'static')));

app.get('/userprofile', (req, res) => {
    const profile = {
        name: 'joao silva',
        birthyear: 1992,
        career: 'professional surfing',
        bio: '<strong>Joao Silva</strong> is one of the top surfers in Brazil.'
    };
    // render the single file template
    res.render('profilefull', profile);
    // render the componentized template
    // res.render('profile', profile);
});

app.get('/userprofile/:id', (req, res) => {
    const profiles = {
        "1": {
            id: 1,
            name: 'joao silva',
            birthyear: 1992,
            career: 'professional surfing',
            bio: '<strong>Joao Silva</strong> is one of the top surfers in Brazil.'
        },
        "2": {
            id: 2,
            name: 'maria santos',
            birthyear: 1987,
            career: 'customer relationship management',
            bio: '<em>Maria Santos</em> is a workaholic who loves cats.'
        }
    };

    if (profiles[req.params.id] == undefined) {
        res.render('error', {
            type: 'unknown_user',
            params: {
                id: req.params.id
            }
        });
    } else {
        res.render('profile', profiles[req.params.id])
    }
});


app.listen(3000, () => {
    console.log('Server listening on port 3000');
});