"use strict";

const express = require('express');
const path = require('path');
const app = express();

// set ejs as template engine
app.set('view engine', 'ejs');
// set the default view lookup folder
app.set('views', path.resolve(__dirname, 'views'));

app.get('/hello/:fname/:lname', (req, res) => {
    res.render('index', {
        fname: req.params.fname,
        lname: req.params.lname
    });
});

app.listen(3000, () => {
    console.log('Server listening on port 3000');
});