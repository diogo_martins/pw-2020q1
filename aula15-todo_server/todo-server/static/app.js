"use strict";

let toDoObjects = [];

function populateNewest() {
    let $content = $("<ul>");
    let toDos = formatToDos();

    for (let i = toDos.length - 1; i >= 0; i--) {
        $content.append($("<li>").text(toDos[i]));
    }

    return $content;
}

function populateOldest() {
    let $content = $("<ul>");
    let toDos = formatToDos();

    for (let i = 0; i < toDos.length; i++) {
        $content.append($("<li>").text(toDos[i]));
    }

    return $content;
}

function populateGroupedByTags() {
    // TODO: fazer essa parte na somativa
}

function populateAdd() {
    let $inputTitle = $("<input>").attr("id", "tag-title").addClass("description"),
        $inputLabel = $("<label>").attr("for", "tag-title").text("Description: "),
        $tagInput = $("<input>").attr("id", "tag-input").addClass("tags"),
        $tagLabel = $("<label>").attr("for", "tag-input").text("Tags: "),
        $button = $("<button>").text("+");

        $button.on("click", function () {
            let description = $inputTitle.val(), 
                tags = $tagInput.val().split(",");
            let toDoObjects = getToDoObjects();
            let toDoItem = {
                "description": description,
                "tags": tags
            };

            save(toDoItem);
            $inputTitle.val("");
            $tagInput.val("");
        });

        return $("<div>").append($inputLabel)
                        .append($inputTitle)
                        .append($tagLabel)
                        .append($tagInput)
                        .append($button);

}

function getToDoObjects() {
    return toDoObjects;
}

/**
 * Load todo items from the server via Ajax
 */
function loadAll() {
    $.getJSON('http://localhost:3000/todos', (data) => {
        toDoObjects = data;
        $(".tabs a span.active").trigger("click");
    });
}

/**
 * Save a todo item in the server
 * After receiving a successful response, 
 * reload the data from the server
 */
function save(todoItem) {
    $.post('http://localhost:3000/todo', todoItem, (res) => {
        if (res.status == 'ok') {
            $.toast('ToDo saved successfully');
            loadAll();
        } else {
            $.toast('Failed to save ToDo at the server');
        }
    });
}

function formatToDos() {
    return getToDoObjects().map(function (todo) {
        return todo.description;
    });
}

$(function () {
    $(".tabs a span").on("click", function () {
        $(".tabs a span").removeClass("active");
        $(this).addClass("active");
        $("main .content").empty();

        let $content;

        if ($(this).is("#newest")) {
            $content = populateNewest();
        } else if ($(this).is("#oldest")) {
            $content = populateOldest();
        } else if ($(this).is("#tags")) {
            $content = populateGroupedByTags();
        } else if ($(this).is("#add")) {
            $content = populateAdd();
        }

        $("main .content").append($content);

        return false;
    });

    loadAll();

    $("#newest").trigger("click");
});
