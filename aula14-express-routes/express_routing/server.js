"use strict";

const http = require('http');
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

let app = express();


http.createServer(app).listen(3000, () => {
    console.log('Listening on port 3000');
});

app.use('/static', express.static([__dirname, 'static'].join(path.sep)));

app.all('/', (req, res, next) => {
    res.write('<p>You made any request');
    next();
});

// GET
app.get('/', (req, res, next) => {
    res.write('<p>You made a GET request');
    res.end();
});

// GET
app.get('/', (req, res) => {
    res.write('<p>You made another GET request');
    res.end();
});

// POST
app.post('/', (req, res, next) => {
    res.write('<p>You made a POST request');
    res.end();
});

// route with pattern matching (nao funcionou)
app.get('/user+details', (req, res) => {
    res.send(req.originalUrl);
});

// route with regular expression
app.get(/ufabc/, (req, res) => {
    res.send(req.originalUrl);
});

// route with query string retrieval
app.get('/query', (req, res) => {
    res.json(req.query);
});

// route with query string validation and response parameterization
app.get('/hello', (req, res) => {
    if (req.query.fname == undefined || req.query.lname == undefined)
        res.send('Required queries: fname and lname');
    else if (req.query.fname.trim() == '' || req.query.lname.trim() == '')
        res.send('Queries must not be empty');
    else
        res.send(`Hello, ${req.query.fname} ${req.query.lname}!`);
});



// middleware to parse request body data
app.use(bodyParser.urlencoded({extended: true}));

// route with request body parsing
app.post('/hello-post', (req, res) => {
    console.log(req.body);
    if (req.body.fname == undefined || req.body.lname == undefined) {
        res.send('<p>All form fields are required');
    } else if (req.body.fname.trim() == '') {
        res.send('<p>First name is required');
    } else if (req.body.lname.trim() == '') {
        res.send('<p>Last name is required');
    } else {
        res.send(`<p>Hello, ${req.body.fname} ${req.body.lname}!`);
    }
});