"use strict";

const ntwitter = require('ntwitter');
const credentials = require('./secrets.json');

let twitter = ntwitter(credentials);
let terms = ['awesome', 'cool'];
let counts = {};

for (let term of terms)
    counts[term] = 0;

twitter.stream(
    'statuses/filter', 
    {'track': terms}, 
    stream => {
        stream.on('data', tweet => {
            for (let term of terms)
                if (tweet.text.indexOf(term) >= 0)
                    counts[term] += 1;
        });
    }  
);

module.exports = counts;
