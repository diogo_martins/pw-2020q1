"use strict";

const express = require('express');
const path = require('path');
const fs = require('fs');
const handlebars = require('express-handlebars');
const bodyParser = require('body-parser');

const STATIC_DIR = path.join(__dirname, 'static');
const TODO_FILE = 'todos.json';
const TOAST_LIB_ROOT = path.join(__dirname, 'node_modules', 
    'jquery-toast-plugin', 'dist');

// in-memory model
let toDoObjects = [];

/**
 * Load the in-memory from disk
 */
function loadFile() {
    fs.readFile(TODO_FILE, (err, data) => {
        if (err) throw err;
        
        toDoObjects = JSON.parse(data);
        console.log(`Data file ${TODO_FILE} loaded successfully`);
    });
}

/**
 * Save the in-memory model to disk
 */
function saveFile() {
    console.log('Saving data to the file system...');
    fs.writeFileSync(TODO_FILE, JSON.stringify(toDoObjects));
    console.log('Finished saving data');
}

/**
 * Convert a list ToDo objects into a list of todo descriptions
 */
function formatToDos() {
    return toDoObjects.map(function (todo) {
        return todo.description;
    });
}

function groupByTags() {
    let todosByTag = {};

    toDoObjects.forEach((todoObj) => {
        todoObj.tags.forEach((tag) => {
            if (todosByTag[tag] == undefined) {
                todosByTag[tag] = [todoObj.description];
            } else {
                todosByTag[tag].push(todoObj.description);
            }
        });
    });

    return todosByTag;
}

const app = express();

/**
 * Start the server and loads the data file
 */
app.listen(3000, () => {
    console.log('ToDo! server SSR MPA listening on port 3000');
    loadFile();
});

/**
 * set handlebars as the view engine
 */
app.engine('handlebars', handlebars({
    helpers: {
        equals: (a, b) => a == b
    }
}));
app.set('view engine', 'handlebars');
app.set('views', path.resolve(__dirname, 'views'));

/**
 * Static route
 */
app.use('/static', express.static(STATIC_DIR));

/**
 * TODO: dynamic routes
 */

app.get('/', (req, res) => {
    res.redirect('/newest');
});

app.get('/newest', (req, res) => {
    res.render('newest', {todos: formatToDos().reverse()});
});

app.get('/oldest', (req, res) => {
    res.render('oldest', {todos: formatToDos()});
});

app.get('/tags', (req, res) => {
    res.render('tags', {tags: groupByTags()})
});

app.get('/add', (req, res) => {
    res.render('add');
});

// configure bodyparser to properly understand post data
app.use(bodyParser.urlencoded({extended: true}));

app.post('/add', (req, res) => {
    let isDataValid = () => 
        req.body.description != undefined 
            && req.body.tags != undefined
            && req.body.description.trim() != ''
            && req.body.tags.trim() != '';
    
    if (isDataValid()) {
        let newTodo = req.body;

        newTodo.tags = newTodo.tags.split(',').map((el) => el.trim());
        toDoObjects.push(newTodo);

        res.render('status', {code: 'succ_add'});
    } else {
        res.render('status', {code: 'err_required_fields'});
    }
});

/**
 * Operating system signal handling
 * Makes the im-memory model be saved o disk when
 * server is interrupted via CTRL+C or nodemon
 */
process.on('exit', (code) => {
    console.log(`Server exiting with code ${code}`);
    saveFile();
});

let exitHandler = (code) => {
    process.exit();
}

process.once('SIGINT', exitHandler);
process.once('SIGUSR2', exitHandler);