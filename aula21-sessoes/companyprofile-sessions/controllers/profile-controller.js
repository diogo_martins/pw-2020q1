"use strict";

const model = require('../models/profile-model.js');
const uploads = require('../conf/config.json').uploads;
const fs = require('fs');
const path = require('path');

exports.index = (req, res) => {
    exports.list(req, res);
};

exports.list = (req, res) => {
    model.UserProfileDAO.listAll(profiles => 
        res.render('list', {profiles: profiles}));
};

exports.addForm = (req, res) => {
    res.render('add', {profile: new model.UserProfile()});
}; 

const getField = (req, field) =>
     (req.fields[field]) ? req.fields[field].pop() : null;

const hasPicture = function(req) {
    return req.files['picture'] !== undefined
};

const saveProfilePicture = function(req, fileSaved) {
    const file = req.files['picture'].pop();
    const filename = path.basename(file.path);
    const newPath = path.join(global.__rootdir, uploads, filename);

    fs.copyFile(file.path, newPath, err => {
        if (err) {
            console.log('Failed to move profile picture file');
            console.log(err.stack);
            fileSaved(null);
        } else {
            fileSaved(filename);
        }
    });
};

exports.addFormProcessing = (req, res) => {
    const saveProfile = function (profile) {
        model.UserProfileDAO.insert(profile, (status) => {
            if (status)
                res.render('status', {type: 'user_add_success'});
            else
            res.render('status', {type: 'user_add_error'});
        });
    };

    const profile = new model.UserProfile();

    profile.name = getField(req, 'name');
    profile.birthdate = getField(req, 'birthdate');
    profile.career = getField(req, 'career');
    profile.bio = getField(req, 'bio');

    if (profile.isValid()) {
        if (hasPicture(req)) {
            saveProfilePicture(req, picture => {
                if (picture !== null) {
                    profile.picture = picture;
                    saveProfile(profile);
                } else {
                    res.render('status', {type: 'user_add_error'});
                }
            });
        } else {
            saveProfile(profile);
        }
    } else {
        res.render('status', {type: 'user_add_error'});
    }
};

exports.details = (req, res) => {
    const id = parseInt(req.params.id);

    model.UserProfileDAO.findById(id, profile => {
        if (profile !== null) {
            res.render('profile', profile);
        } else {
            res.render('status', {
                type: 'unknown_user',
                params: {
                    id: req.params.id
                }
            });
        }
    });
};

exports.editForm = (req, res) => {
    const id = parseInt(req.params.id);

    model.UserProfileDAO.findById(id, profile => {
        if (profile !== null) {
            res.render('edit', {profile: profile});
        } else {
            res.render('status', {type: 'unknown_user'});
        }
    });
}

exports.editFormProcessing = (req, res) => {
    const saveProfile = function(profile) {
        model.UserProfileDAO.update(profile, status => {
            if (status) {
                req.session.flash = {
                    type: 'edit-success'
                };
            } else {
                req.session.flash = {
                    type: 'edit-error'
                }
            }
            res.redirect(`profile/${profile.id}`);
        });
    };

    const profile = new model.UserProfile();

    profile.id = parseInt(getField(req, 'id'));
    profile.name = getField(req, 'name');
    profile.birthdate = getField(req, 'birthdate');
    profile.career = getField(req, 'career');
    profile.bio = getField(req, 'bio');

    if (profile.isValid()) {
        if (hasPicture(req)) {
            saveProfilePicture(req, picture => {
                if (picture !== null) {
                    profile.picture = picture;
                    saveProfile(profile);
                } else {
                    res.render('status', {type: 'user_add_error'});
                }
            });
        } else {
            saveProfile(profile);
        }
    } else {
        res.render('stats', {type: 'invalid_form'});
    }
};