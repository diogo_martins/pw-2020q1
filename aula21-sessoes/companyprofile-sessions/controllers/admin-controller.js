"use strict";

const model = require('../models/admin-model.js');
const bcrypt = require('bcrypt');

exports.loginForm = (req, res) => {
    res.render('login');
};

exports.loginFormProcessing = (req, res) => {
    const admin = new model.Admin();

    admin.email = req.body.email;
    admin.password = req.body.password;

    if (admin.isValid()) {
        model.AdminDAO.findByEmail(admin.email, retrAdmin => {
            if (retrAdmin !== null) {
                bcrypt.compare(admin.password, retrAdmin.password, 
                    (err, matched) => {
                        if (matched) {
                            req.session.authenticated = true;
                            req.session.flash = {
                                type: 'login',
                                name: retrAdmin.name
                            }
                        } 
                        res.redirect('/');
                    });
            } else {
                req.session.authenticated = false;
                res.render('status', {type: "invalid_login"});
            }
        });
    } else {
        res.render('status', {type: 'invalid_login'});
    }
};

exports.logout = (req, res) => {
    if (req.session.authenticated) {
        req.session.authenticated = false;
        req.session.flash = {
            type: 'logout'
        }
    }
    res.redirect('/');
};