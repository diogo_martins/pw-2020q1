"use strict";

global.__rootdir = __dirname;

const express = require('express');
const process = require('process');
const handlebars = require('express-handlebars');
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');

const dbConnect = require('./models/db-connect.js');
const profileController = require('./controllers/profile-controller.js');
const adminController = require('./controllers/admin-controller.js');
const config = require('./conf/config.json');
const multiparty = require('multiparty-express').multipartyExpress;

const app = express();

dbConnect.connect(() => {
    console.log('Database connected');
    app.listen(3000, () => {
        console.log('Server listening on port 3000');
    });
});

process.on('exit', (code) => {
    console.log(`Server exiting with code ${code}`);
    dbConnect.disconnect(() => {
        console.log('Database disconnected');
    });
});

let exitHandler = (code) => {
    process.exit();
}

// configure the session middlware
app.use(session({
    secret: config.secret,
    resave: false, 
    saveUninitialized: false,
    store: dbConnect.sessionStore
}));

process.once('SIGINT', exitHandler);
process.once('SIGUSR2', exitHandler);

// custom middlewares
app.use((req, res, next) => {
    res.locals.session = req.session;
    res.locals.flash = req.session.flash;
    delete req.session.flash;
    next();
});

app.engine('handlebars', handlebars({
    helpers: {
        capitalize: (s) => s.split(' ').map((e) => 
                        e.charAt(0).toUpperCase() + e.slice(1).toLowerCase())
                        .join(' '),
        userAge: (birthyear) => 
            (new Date()).getFullYear() - birthyear, 
        parseYear: (birthdate) => (new Date(Date.parse(birthdate))).getFullYear(),
        formatDate: date => {
            if (date !== undefined)
                return (new Date(Date.parse(date))).toISOString().slice(0,10);
        },
        equals: (a, b) => a == b
    }
}));
app.set('view engine', 'handlebars');
app.set('views', path.resolve(__dirname, 'views'));

// 3rd-party middlwares
app.use(bodyParser.urlencoded({extended: true}));

function authenticate(req, res, next) {
    if (req.session.authenticated)
        next();
    else
        res.redirect('/login');
}

// static routes
app.use('/static', express.static(path.join(__dirname, 'static')));
app.use('/lib/bootstrap', express.static(
    path.join(__dirname, 'node_modules', 'bootstrap', 'dist')));
app.use('/lib/jquery', express.static(
    path.join(__dirname, 'node_modules', 'jquery', 'dist')));
app.use('/pictures', express.static(path.join(__dirname, 'uploads')));

// dynamic routes
// bind dynamic routes to controller actions
app.get('/', profileController.index);
app.get('/list', profileController.list);
app.get('/add', authenticate, profileController.addForm);
app.post('/add', authenticate, multiparty(), 
    profileController.addFormProcessing);
app.get('/profile/:id', profileController.details);

app.get('/login', adminController.loginForm);
app.post('/login', adminController.loginFormProcessing);
app.get('/logout', adminController.logout);

app.get('/edit/:id', authenticate, profileController.editForm);
app.post('/edit', authenticate, multiparty(), 
    profileController.editFormProcessing);