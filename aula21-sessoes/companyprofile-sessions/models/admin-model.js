"use strict";

const colls = require("./db-connect").colls;

/**
 * Transfer object
 */
function Admin(email, name, password) {
    this.id = null;
    this.email = email;
    this.name = name;
    this.password = password;
}

/**
 * The object is valid if email and password and not null and 
 * not empty
 */
Admin.prototype.isValid = function () {
    const reducer = (acc, cur) =>
        acc && cur !== undefined && cur !== null && cur.trim() != '';

    return [this.email, this.password].reduce(reducer, true);
};

const AdminDAO = {};

AdminDAO.toObj = function(doc) {
    const admin = new Admin();

    admin.id = doc.id;
    admin.email = doc.email;
    admin.name = doc.name;
    admin.password = doc.password;

    return admin;
}

AdminDAO.findByEmail = function(email, sendResult) {
    colls.admins.findOne({email: email}, (err, res) => {
        if (err !== null) {
            console.log(err.stack);
            sendResult(null);
        } else if (res == null) {
            sendResult(res);
        } else {
            sendResult(AdminDAO.toObj(res));
        }
    });
};

exports.Admin = Admin;
exports.AdminDAO = AdminDAO;