"use strict";

const dbConnect = require('./db-connect.js');
const model = require('./admin-model.js');

function testFindById() {
    const id = 'admin@email.com';

    dbConnect.connect(() => {
        model.AdminDAO.findByEmail(id, (admin) => {
            console.log('Restrieved admin: ');
            console.log(admin);
            dbConnect.disconnect();
        });
    });
}

testFindById();