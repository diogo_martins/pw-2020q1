"use strict";

const dbConnect = require('./db-connect.js');
const model = require('./profile-model.js');

const profiles = [
    new model.UserProfile(
        "joao silva",
        '01/01/1992',
        'professional surfing',
        '<b>Joao Silva</b> is one the top surfers in Brazil.'),
    new model.UserProfile(
        "maria santos",
        '01/01/1987',
        'customer relationship management',
        '<em>Maria Santos</em> is a workaholic who loves cats.'
    )
];

function testInsert() {
    dbConnect.connect(() => {
        let remaining = profiles.length;

        profiles.forEach(profile =>
            model.UserProfileDAO.insert(profile, (status) => {
                console.log('Inserting element.');
                console.log(`Status: ${status}`);
                remaining--;
                if (remaining == 0)
                    dbConnect.disconnect();
            })
        );
    });
}

function testListaAll() {
    dbConnect.connect(() => {
        model.UserProfileDAO.listAll((docs) => {
            console.log('Listing all elements');
            console.log(docs);
            dbConnect.disconnect();
        });
    });
}

function testFindById() {
    const id = 3;

    dbConnect.connect(() => {
        model.UserProfileDAO.findById(id, (profile) => {
            console.log('Restrieved profile: ');
            console.log(profile);
            dbConnect.disconnect();
        });
    });
}

function testUpdate() {
    const id = 2;

    dbConnect.connect(() => {
        console.log(`Retrieving profile id ${id}`);
        model.UserProfileDAO.findById(id, profile => {
            profile.name = "Mariana Silva";
            console.log(`Updating profile id ${id}`);
            model.UserProfileDAO.update(profile, status => {
                console.log('Status: ');
                console.log(status);
                model.disconnect();
            });
        });
    });
}

// testInsert();
// testListaAll();
// testFindById();
testUpdate();