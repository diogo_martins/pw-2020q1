"use strict";

const colls = require('./db-connect.js').colls;

/**
 * Data object or transfer object
 * 
 */
function UserProfile(name, birthdate, career, bio, picture) {
    this.id = null;
    this.name = name;
    this.birthdate = birthdate;
    this.career = career;
    this.bio = bio;
    this.picture = picture;
}

function nextId(idReady) {
    colls.sequences.findOneAndUpdate({name: 'profile_id'}, {$inc: {value: 1}}, 
        (err, res) => {
            if (err !== null) {
                console.log(err);
            }
            idReady(res.value.value);
        });
}

/**
 * Validate the object by checking if all static values
 * are defined, non-null and non-empty
 */
UserProfile.prototype.isValid = function() {
    const reducer = (acc, cur) => 
        acc && cur !== undefined && cur !== null && cur.trim() != '';

    return [this.name, this.birthdate, this.career, this.bio]
        .reduce(reducer, true);
};

const UserProfileDAO = {};

/**
 * Converts a profile object to an object literal
 * It is necessary currently, but it will help
 * in the evolution of the db, if we decide
 * to change names later (decoupling between the layers)
 */
UserProfileDAO.toDoc = function (profile) {
    return {
        id: profile.id,
        name: profile.name,
        birthdate: profile.birthdate,
        career: profile.career,
        bio: profile.bio,
        picture: profile.picture
    }
}

/**
 * Convert a doc to a transfer object
 */
UserProfileDAO.toObj = function(doc) {
    const profile = new UserProfile();

    profile.id = doc.id;
    profile.name = doc.name;
    profile.birthdate = doc.birthdate;
    profile.career = doc.career;
    profile.bio = doc.bio;
    profile.picture = doc.picture;
    
    return profile;
}

UserProfileDAO.insert = (userProfile, sendStatus) => {
    nextId((id) => {
        if (id === null) {
            console.log('Failed to generate a profile id');
            sendStatus(false);
        } else {
            userProfile.id = id;
            colls.profiles.insertOne(UserProfileDAO.toDoc(userProfile), 
                (err, res) => {
                    if (err === null) {
                        sendStatus(res.insertedCount > 0);
                    } else {
                        console.log(err.stack);
                        sendStatus(false);
                    }
                }
            );
        }
    });
};

UserProfileDAO.listAll = (sendResult) => {
    colls.profiles.find({}, {projection: {_id: 0}}).toArray((err, docs) => {
        if (err === null) {
            const profiles = [];

            docs.forEach(doc => {
                const profile = UserProfileDAO.toObj(doc);

                profile.id = doc.id;
                profiles.push(profile);
            });
            sendResult(profiles);
        } else {
            console.log(err.stack);
            sendResult([]);
        }
    });
};

UserProfileDAO.findById = (id, sendResult) => {
    colls.profiles.findOne({id: id}, (err, res) => {
        if (err !== null) {
            console.log(err.stack);
            sendResult(null);
        } else {
            const profile = UserProfileDAO.toObj(res);
            profile.id = res.id;
            sendResult(profile);
        }
    });
};

UserProfileDAO.update = (profile, sendStatus) => {
    colls.profiles.replaceOne(
        {id: profile.id},
        UserProfileDAO.toDoc(profile),
        (err, res) => {
            if (err === null) {
                sendStatus(res.matchedCount > 0);
            } else {
                console.log(err.stack);
                sendStatus(false);
            }
        }
    );
};

module.exports = {
    UserProfile: UserProfile,
    UserProfileDAO: UserProfileDAO
}
