window.onload = function() {
    var hourOfDay = (new Date()).getHours(); 
    var greeting; 

    if (hourOfDay > 4 && hourOfDay < 12) {
        greeting = "Good Morning";
    }
    else if (hourOfDay >= 12 && hourOfDay < 18) {
        greeting = "Good Afternoon";
    }
    else {
        greeting = "Good Evening";
    }

    alert(greeting);
}

