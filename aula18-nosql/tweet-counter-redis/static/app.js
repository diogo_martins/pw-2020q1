"use strict";

const URL = 'http://localhost:3000/counts';

Handlebars.registerHelper('hasData', 
    counts => Object.keys(counts).length > 0);

function main() {
    $.getJSON(URL, counts => {
        /**
         * Render the table from the handlebars template using
         * the json data retrieved from the server
         * Alternatively, the template could be downloaded from 
         * the server via Ajax
         */
        const template = Handlebars.compile($('#table-template').html());
        const htmlContent = template({counts: counts});

        $('main').html(htmlContent);

        /**
         * Update counts
         */
        setInterval(() => {
            $.getJSON(URL, counts => {
                for (const term in counts)
                    $(`#${term}`).text(counts[term]);
            });
        }, 1000);
    });
}

$(document).ready(main);