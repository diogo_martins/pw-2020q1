/***
 * Mongo shell script to create an empty profiles db
 */

 db = connect('127.0.0.1:27017/associated-consulting');
 db.dropDatabase();
 db = connect('127.0.0.1:27017/associated-consulting');
 db.createCollection('profiles');
 db.profiles.createIndex(['id'], {unique: true});
 db.createCollection('sequences');
 db.sequences.insertOne({
     name: 'profile_id',
     value: 1
 });