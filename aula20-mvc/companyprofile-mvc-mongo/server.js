"use strict";

const express = require('express');
const process = require('process');
const handlebars = require('express-handlebars');
const path = require('path');
const bodyParser = require('body-parser');

const profileModel = require('./models/profile-model.js');
const profileController = require('./controllers/profile-controller.js');

const app = express();

app.listen(3000, () => {
    console.log('Server listening on port 3000');
    profileModel.connect(() => {
        console.log('Database connected');
    });
});

process.on('exit', (code) => {
    console.log(`Server exiting with code ${code}`);
    profileModel.disconnect(() => {
        console.log('Database disconnected');
    });
});

let exitHandler = (code) => {
    process.exit();
}

process.once('SIGINT', exitHandler);
process.once('SIGUSR2', exitHandler);

app.engine('handlebars', handlebars({
    helpers: {
        capitalize: (s) => s.split(' ').map((e) => 
                        e.charAt(0).toUpperCase() + e.slice(1).toLowerCase())
                        .join(' '),
        userAge: (birthyear) => 
            (new Date()).getFullYear() - birthyear, 
        parseYear: (birthdate) => (new Date(Date.parse(birthdate))).getFullYear(),
        equals: (a, b) => a == b
    }
}));
app.set('view engine', 'handlebars');
app.set('views', path.resolve(__dirname, 'views'));

// static routes
app.use('/static', express.static(path.join(__dirname, 'static')));
app.use('/lib/bootstrap', express.static(
    path.join(__dirname, 'node_modules', 'bootstrap', 'dist')));
app.use('/lib/jquery', express.static(
    path.join(__dirname, 'node_modules', 'jquery', 'dist')));

// dynamic routes
// bind dynamic routes to controller actions
app.get('/', profileController.index);
app.get('/list', profileController.list);
app.get('/add', profileController.addForm);
app.use(bodyParser.urlencoded({extended: true}));
app.post('/add', profileController.addFormProcessing);
app.get('/profile/:id', profileController.details);