"use strict";

const model = require('../models/profile-model.js');

exports.index = (req, res) => {
    res.redirect('/list');
};

exports.list = (req, res) => {
    model.UserProfileDAO.listAll(profiles => 
        res.render('list', {profiles: profiles}));
};

exports.addForm = (req, res) => {
    res.render('add');
}; 

exports.addFormProcessing = (req, res) => {
    const profile = new model.UserProfile(
        [req.body.fname, req.body.lname].join(' '),
        req.body.birthdate,
        req.body.career,
        req.body.bio
    );

    model.UserProfileDAO.insert(profile, (status) => {
        if (status)
            res.render('status', {type: 'user_add_success'});
        else
        res.render('status', {type: 'user_add_error'});
    });
};

exports.details = (req, res) => {
    const id = parseInt(req.params.id);

    model.UserProfileDAO.findById(id, profile => {
        if (profile !== null) {
            res.render('profile', profile);
        } else {
            res.render('status', {
                type: 'unknown_user',
                params: {
                    id: req.params.id
                }
            });
        }
    });
};