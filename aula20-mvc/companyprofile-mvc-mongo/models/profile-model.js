"use strict";

const dbConf = require('../conf/db.json');
const MongoClient = require('mongodb').MongoClient;

const client = new MongoClient(dbConf.url, {useUnifiedTopology: true});
let db = null;
let profileColl = null;

/**
 * Connect to the database and warns any listeners
 * that the connection is ready
 */
function connect(connectionReady) {
    client.connect((err) => {
        if (err === null) {
            db = client.db(dbConf.db);
            profileColl = db.collection(dbConf.colls.profiles);
            connectionReady();
        } else {
            console.log('Failed to connect to the db');
            console.log(err.stack);
        }
    });
}

function disconnect(disconnected) {
    if (client !== null && client.isConnected()) {
        client.close(() => {
            if (disconnected !== undefined)
                disconnected();
        });
    }
}

/**
 * Data object or transfer object
 * 
 * @param {*} name 
 * @param {*} birthdate 
 * @param {*} career 
 * @param {*} bio 
 */
function UserProfile(name, birthdate, career, bio) {
    this.id = null;
    this.name = name;
    this.birthdate = birthdate;
    this.career = career;
    this.bio = bio;
}

function nextId(idReady) {
    const seqColl = db.collection(dbConf.colls.sequences);

    seqColl.findOneAndUpdate({name: 'profile_id'}, {$inc: {value: 1}}, 
        (err, res) => {
            if (err !== null) {
                console.log(err);
            }
            idReady(res.value.value);
        });
}

const UserProfileDAO = {};

/**
 * Converts a profile object to an object literal
 * It is necessary currently, but it will help
 * in the evolution of the db, if we decide
 * to change names later (decoupling between the layers)
 */
UserProfileDAO.toDbLiteral = function (profile) {
    return {
        id: profile.id,
        name: profile.name,
        birthdate: profile.birthdate,
        career: profile.career,
        bio: profile.bio
    }
}

UserProfileDAO.insert = (userProfile, sendStatus) => {
    nextId((id) => {
        if (id === null) {
            console.log('Failed to generate a profile id');
            sendStatus(false);
        } else {
            userProfile.id = id;
            profileColl.insertOne(UserProfileDAO.toDbLiteral(userProfile), 
                (err, res) => {
                    if (err === null) {
                        sendStatus(res.insertedCount > 0);
                    } else {
                        console.log(err.stack);
                        sendStatus(false);
                    }
                }
            );
        }
    });
};

UserProfileDAO.listAll = (sendResult) => {
    profileColl.find({}, {projection: {_id: 0}}).toArray((err, docs) => {
        if (err === null) {
            const profiles = [];

            docs.forEach(doc => {
                const profile = new UserProfile(
                    doc.name,
                    doc.birthdate,
                    doc.career,
                    doc.bio
                );
                profile.id = doc.id;
                profiles.push(profile);
            });
            sendResult(profiles);
        } else {
            console.log(err.stack);
            sendResult([]);
        }
    });
};

UserProfileDAO.findById = (id, sendResult) => {
    profileColl.findOne({id: id}, (err, res) => {
        if (err !== null) {
            console.log(err.stack);
            sendResult(null);
        } else {
            const profile = new UserProfile(
                res.name,
                res.birthdate,
                res.career,
                res.bio
            );
            profile.id = res.id;
            sendResult(profile);
        }

        
    });
};

// conectar
// consultas (CRUD)
// desconectar

module.exports = {
    connect: connect,
    disconnect: disconnect,
    UserProfile: UserProfile,
    UserProfileDAO: UserProfileDAO
}
