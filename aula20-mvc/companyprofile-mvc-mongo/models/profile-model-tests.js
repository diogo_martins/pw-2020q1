"use strict";

const model = require('./profile-model.js');

const profiles = [
    new model.UserProfile(
        "joao silva",
        '01/01/1992',
        'professional surfing',
        '<b>Joao Silva</b> is one the top surfers in Brazil.'),
    new model.UserProfile(
        "maria santos",
        '01/01/1987',
        'customer relationship management',
        '<em>Maria Santos</em> is a workaholic who loves cats.'
    )
];

function testInsert() {
    model.connect(() => {
        let remaining = profiles.length;

        profiles.forEach(profile =>
            model.UserProfileDAO.insert(profile, (status) => {
                console.log('Inserting element.');
                console.log(`Status: ${status}`);
                remaining--;
                if (remaining == 0)
                    model.disconnect();
            })
        );
    });
}

function testListaAll() {
    model.connect(() => {
        model.UserProfileDAO.listAll((docs) => {
            console.log('Listing all elements');
            console.log(docs);
            model.disconnect();
        });
    });
}

function testFindById() {
    const id = 3;

    model.connect(() => {
        model.UserProfileDAO.findById(id, (profile) => {
            console.log('Restrieved profile: ');
            console.log(profile);
            model.disconnect();
        });
    });
}

// testInsert();
testListaAll();
// testFindById();